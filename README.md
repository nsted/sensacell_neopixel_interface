F-Minus Snowflake Project
By Nicholas Stedman
Fall 2014

neopixel_firmware Controls NeoPixel LED Strips based on the 
activity of neighbouring Sensacell Pixels.

The due_serial_relay.ino takes in Sensacell data over RS485 
and spits it out to Mega to control LEDs. Mega can't reliably 
talk at 230400 baud, the required rate of the Sensacells.