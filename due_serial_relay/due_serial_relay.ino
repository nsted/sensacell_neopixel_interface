/************************************************
Due Serial RelayDue Serial Relay

F-Minus Snowflake Project
By Nicholas Stedman
Fall 2014

Takes in the data on the Due over RS485 
and spits it out to Mega to control LEDs.
Mega can't reliably speak at 230400 baud,
the required rate of the Sensacells.
************************************************/

void setup() {
  Serial2.begin(230400);
  Serial2.flush();
  Serial3.begin(250000);
  Serial3.flush();
}


void loop() {
  while(Serial2.available()){
    char buff = Serial2.read();   
    Serial3.write(buff);
  }
}


