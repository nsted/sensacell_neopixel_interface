/************************************************
Due Serial RelayDue Serial Relay

F-Minus Snowflake Project
By Nicholas Stedman
Fall 2014

Takes in the data on the Due over RS485 
and spits it out to Mega to control LEDs.
Mega can't reliably speak at 230400 baud,
the required rate of the Sensacells.
************************************************/

char command[7] = {'0','0','9','1','a','0','0'};
const int packetLength = 580;
char buffer[packetLength];
int timeStamp;

void setup() {
  Serial2.begin(230400);
  Serial2.flush();
  Serial3.begin(250000);
  Serial3.flush();

  // random buffer
  for(int i = 0; i < packetLength; i++) {
    buffer[i] = ((char)(random(15))) + '0';
  }  
}


void loop() {

  // send the sensor command
  for(int i = 0; i < 7; i++) {
    Serial3.write(command[i]);
  }
  Serial3.write(0x0D);

  delayMicroseconds(500);

  // send the sensor values

  // send all active
  if((millis() - timeStamp) < 2000) { 
    for(int i = 0; i < packetLength; i++) {
      Serial3.write('F');
    }
    Serial3.write(0x0D);
  } 
  // send all idle
  else if((millis() - timeStamp) < 10000) { 
    for(int i = 0; i < packetLength; i++) {
      Serial3.write('0');
    }
    Serial3.write(0x0D);
  } 
  // send alternating
  else if((millis() - timeStamp) < 12000) { 
    for(int i = 0; i < packetLength; i+=2) {
      Serial3.write('0');
      Serial3.write('F');      
    }
    Serial3.write(0x0D);
  }    
  // send all idle
  else if((millis() - timeStamp) < 20000) { 
    for(int i = 0; i < packetLength; i++) {
      Serial3.write('0');
    }
    Serial3.write(0x0D);
  }   
//  // send all random
//  else if((millis() - timeStamp) < 22000) { 
//    for(int i = 0; i < packetLength; i++) {
//      Serial3.write(buffer[i]);
//    }
//    Serial3.write(0x0D);
//  }  
//  // send all idle
//  else if((millis() - timeStamp) < 30000) { 
//    for(int i = 0; i < packetLength; i++) {
//      Serial3.write('0');
//    }
//    Serial3.write(0x0D);
//  }   
  else {
    timeStamp = millis();
  }

  delay(100);
  
}


