/************************************************
Due Serial Subset TEst

F-Minus Snowflake Project
By Nicholas Stedman
Fall 2014

Takes in the sensacell packet on the Due over RS485 
and spits it out to Mega, followed immediately by 
the remaining bytes in the packet for the entire 
Snowflake. Thus a complete simulation, with some
live interaction on a subset of tiles.

Used for testing on a subset of tiles in studio.
Gives a sense of actual data load of the full sculpture.
************************************************/

const int buffLength = 580;       // 145 tiles * 4 rows per tile
char buffer[buffLength];

void setup() {
  Serial.begin(250000);
  Serial.flush();  
  Serial2.begin(230400);
  Serial2.flush();
  Serial3.begin(250000);
  Serial3.flush();
}


void loop() {
  //read the packet, whatever the length
  int packetLength = Serial2.readBytesUntil(0x0D, buffer, buffLength);
//  Serial.println(packetLength);

  // if the packet is a command, then just write it
  if(packetLength == 7) {
    for(int i = 0; i < 7; i++) {
      Serial3.write(buffer[i]);
      Serial.write(buffer[i]);      
    }  
    Serial3.write(0x0D);
    Serial.println();    
  }

  //write the available sensor values, and fill in the gap to emulate a complete sculpture packet
  else {
    int packetIndex;  
    for(packetIndex = 0; packetIndex < packetLength; packetIndex++) {
      Serial3.write(buffer[packetIndex]);
      Serial.write(buffer[packetIndex]);      
    }
    //emulate the remaining byte
    for(int i = packetIndex; i < buffLength; i++) {
      Serial3.write('0');
      Serial.write('0');      
    }
    Serial3.write(0x0D);
    Serial.println();     
  }
}


