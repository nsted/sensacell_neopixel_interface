/* 
Prints out a lookup table that correlates a Neopixel with a Sensacell pixel.
Needed to save memory in the Arduino.
Run, copy and paste the printout to "neopixel_firmware.ino"
*/



//** Sensacell LED matching the first NeoPixel on each strip is given
//** Determined by multiplying the Tile # by 16 LEDs per Tile, and offsetting to the particular LED.
int[] pxl = 
{
  9*16-4,                           // strip: 0
  9*16-1,                           // strip: 1
  11*16-8+3,                        // strip: 2
  25*16-8,                          // strip: 3
  11*16,                            // strip: 4
  25*16+3,                          // strip: 5
  10*16+4+3                             // strip: 6
};

int z = 0;                          // To advance through all the first LEDs (pxl) 

//** For each NeoPixel, determine the Sensacell LED it's closest to.
//** Start at the given first LED, and walk to the last pixel.
int[][] scLookup = {
  {
    pxl[z], pxl[z], // Tile 9
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 8
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 7
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 6
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 5
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 4
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 3
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], // Tile 2 (2 rows only)
    pxl[z]-=4, pxl[z], pxl[z],
    pxl[z]-=(4*6), pxl[z]+=1, pxl[z], pxl[z]+=1, pxl[z], pxl[z]+=1, pxl[z], pxl[z]    
  }
  , 
  {
    pxl[++z], pxl[z], // Tile 9
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 8
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 7
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 6
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 5
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 4
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], // Tile 3
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z], // Tile 2 (2 rows only)
    pxl[z]-=4, pxl[z], pxl[z],
    pxl[z]-=(4*6), pxl[z]-=1, pxl[z], pxl[z]-=1, pxl[z], pxl[z]-=1, pxl[z], pxl[z]
  }
  , 
  {
    pxl[++z], pxl[z], // Tile 11
    pxl[z]+=8, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z],  // Tile 12
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],  // Tile 13
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 14 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 15 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], // Tile 16 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], // Tile 17 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
//    pxl[z]+=4, pxl[z], // Tile 18  
    pxl[z]+=(4*11-1), pxl[z], pxl[z]-=1, pxl[z], pxl[z]-=1, pxl[z], // Tile 23
    pxl[z], pxl[z], pxl[z]+=1, pxl[z],
  }
  , 
  {
    pxl[++z], pxl[z], // Tile 11
    pxl[z]+=8, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z],  // Tile 12
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],  // Tile 13
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 14 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 15 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], // Tile 16 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], // Tile 17 
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
//    pxl[z]+=4, pxl[z], // Tile 18  
    pxl[z]+=(4*11-1), pxl[z], pxl[z]-=1, pxl[z], pxl[z]-=1, pxl[z], // Tile 23
    pxl[z], pxl[z], pxl[z]+=1, pxl[z],
  }
  , 
  {
    pxl[++z], pxl[z], // Tile 12
    pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 13
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 14
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 15
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 16
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 17
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z],   
    pxl[z]+=(4*3), pxl[z], // Tile 21
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 22
    pxl[z]+=4, pxl[z],  pxl[z], 
    pxl[z]+=(4*19-1), pxl[z], pxl[z]-=1, pxl[z], pxl[z]-=1, pxl[z], // Tile 34
    pxl[z]++, pxl[z]++, pxl[z], pxl[z]    
  }
  , 
  {
    pxl[++z], pxl[z], // Tile 26
    pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 27
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 28
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 29
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 30
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 31
    pxl[z]+=4, pxl[z],
    pxl[z]+=4, pxl[z], pxl[z],    
    pxl[z]+=(4*3), pxl[z], // Tile 35
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]+=4, pxl[z], // Tile 36
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=(4*18-3), pxl[z], pxl[z]+=1, pxl[z], pxl[z]+=1, pxl[z], // Tile 20
    pxl[z]--, pxl[z]--, pxl[z]
  }
  ,   
  {
    pxl[++z], pxl[z], // Tile 11
    pxl[z]+=4, pxl[z], pxl[z], 
    pxl[z]+=4, pxl[z], 
    pxl[z]-=2, pxl[z],
    pxl[z]-=1, pxl[z],
    pxl[z]+=((14*16)+3), pxl[z],  // Tile 25
    pxl[z]-=1, pxl[z],
    pxl[z]-=2, pxl[z],
    pxl[z]-=4, pxl[z], pxl[z],
    pxl[z]-=4, pxl[z], 
    pxl[z]-=4, pxl[z], pxl[z],
  }
};


println("{");
for (int i = 0; i < 7; i++) {       // for all the strips
  int[] strip = scLookup[i];
  println("{");
  for (int led = 0; led < strip.length; led++) {            // and all the leds on that strip
    //    print( led + "," );
    if (led == strip.length-1) {
    println(strip[led]);
    } else {
    print(strip[led] + ",");
    }    
  }
  if (i == 6) {
    println("}");
  } else {
    println("},");
  }
};
println("};");




