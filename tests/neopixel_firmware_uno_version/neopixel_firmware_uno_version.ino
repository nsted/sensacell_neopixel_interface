#include <avr/pgmspace.h>
#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>
#include "neopixel_firmware.h"



//** init
void setup()  
{      
  initStrips();
  initColors();
  setStripsColor(idleL2_color);  
  setColorOffsets();      
  showStrips();

  Serial.begin(115200);
  Serial.println("hello");

//  Serial3.begin(250000);
//  Serial3.flush(); 
  

#ifdef DEBUG
  test();
#endif
}


//** main loop
void loop(){
  static long offsetTimestamp = millis();
  static long fadeTimestamp = micros();
  static long fadeTimer = 0;
  static int _led = 0;
  static int _strip = 0;  

//  //** each loop update an led...and then check for serial data
//  if(_led == strips[_strip].numPixels()){
//    strips[_strip].show();
//    _led = 0;
//    _strip++;
//    if(_strip == STRIPS_TOTAL){
//      _strip = 0;
//    }
//  }
//  fadeDown(strips[_strip], _led);
//  _led++;

  testPixels();

//  //** check the pots when the timer goes off
//  if(millis() - offsetTimestamp > 100){
//    fadeRate = (1023 - analogRead(FADE_TIME_POT))/64 + 1;
//    setColorOffsets();
//    showStrips();
//    offsetTimestamp = millis();
//  }


  //** check for serial data
  while(Serial.available()){
    //** read in the packet & see if it's a command
    int bufferLength = Serial.readBytesUntil(0x0D, raw, MAX_BUFFER_LENGTH);    
//    printBuffer(raw, bufferLength);                // for debugging only
    int command = commandCheck(raw);

    //** if it's a "color mode pointer" command then we are setting the color values for the sculpture
    if(command == COLOR_POINTER_COMMAND){
      int pointer = getByte(raw, 2);

      bufferLength = Serial.readBytesUntil(0x0D, raw, MAX_BUFFER_LENGTH); 
      command = commandCheck(raw);

      //** the next command should be the "color value", containing the color to load
      if(command == COLOR_VALUE_COMMAND){    
        int value = getByte(raw, 2);
        parseColor(pointer, value);
      }
    }

    //** if it's a sensor read command, then collect the sensor data, and trigger the right leds
    else if(command == SENSOR_READ_COMMAND){ 
      bufferLength = Serial.readBytesUntil(0x0D, raw, MAX_BUFFER_LENGTH);
//      printBuffer(raw, bufferLength);                    // for debugging only
      reverseByteOrder(raw, bufferLength);               // only use if Pixel order per Sensacell Tile is reversed                
      lightUp(raw, bufferLength);                        // got the Sensacell Sensor data, now go light up the NeoPixels
    }
  }
}


//** lookup states of Sensacell pixels, and turn on corresponding NeoPixel
void lightUp(char scPxls[], int packetsReceived){
  for(int strip = 0; strip < STRIPS_TOTAL; strip++){       // for all the strips
    for(int led = 0; led < strips[strip].numPixels(); led++){            // and all the leds on that strip
      int stripToLookup = strip % STRIPS_PER_FACE;
      
      int scPxl = pgm_read_word_near(&(scLookup[stripToLookup][led]));
//      int scPxl = scLookup[stripToLookup][led];

      //** Shift the Value of the Sensacell Pixel to the face it's on
      if( (strip >= 21) && (strip <= 27) ){                    // green face
        scPxl += TILES_PER_FACE * SENSORS_PER_TILE;
      } else if( (strip >= 0) && (strip <= 6) ){               // blue face
        scPxl += TILES_PER_FACE * SENSORS_PER_TILE * 2;        
      } else if( (strip >= 7) && (strip <= 13) ){              // orange face
        scPxl += TILES_PER_FACE * SENSORS_PER_TILE * 3;        
      }

      int pxlState = getScPxlState(scPxl,scPxls,packetsReceived);        //  look up state of matching sensacell pixel

      if( pxlState == 1 ){                                 //  match its color state
        //        Serial.println(led);
        strips[strip].setPixelColor(led, activeL2_color);
      }
      else {     
        //        Serial.print(strip);
        //        Serial.print(',');        
        //        Serial.println(led);        
//        strips[strip].setPixelColor(led, idleL2_color);
        fadeDown(strips[strip], led);
      } 
    }
    strips[strip].show();                                  //  make it so... 
  }
}


//** lookup states of Sensacell pixels, and turn on corresponding NeoPixel
void fadeDown(Adafruit_NeoPixel& strip, int led){
  uint32_t _color = strip.getPixelColor(led);
  if(_color != idleL2_color){

    int _red = (_color >> 16) & 0xFF;
    int _green = (_color >> 8) & 0xFF;
    int _blue = _color & 0xFF;         

    if(_red > idleL2[0]){
      if( (_red - idleL2[0]) > fadeRate){
        _red-=fadeRate;
      } else {
        _red--;        
      }
//          Serial.println("r-");
    } 
    else if(_red < idleL2[0]){
      if( (idleL2[0] - _red) > fadeRate){
        _red+=fadeRate;
      } else {
        _red++;           
      } 
//          Serial.println("r+");
    }
    if(_green > idleL2[1]){
      if( (_green - idleL2[1]) > fadeRate){
        _green-=fadeRate;
      } else {
        _green--;        
      }
//          Serial.println("g-");          
    } 
    else if(_green < idleL2[1]){
      if( (idleL2[1] - _green) > fadeRate){
        _green+=fadeRate;
      } else {
        _green++;           
      } 
//          Serial.println("g+");          
    }
    if(_blue > idleL2[2]){
      if( (_red - idleL2[2]) > fadeRate){
        _blue-=fadeRate;
      } else {
        _blue--;           
      }           
//          Serial.println("b-");          
    } 
    else if(_blue < idleL2[2]){
      if( (idleL2[2] - _red) > fadeRate){    
        _blue+=fadeRate;
      } else {
        _blue++;           
      } 
//          Serial.println("b+");          
    }

    constrain(_red,0,255);
    constrain(_green,0,255);
    constrain(_blue,0,255);
    
    _color = strip.Color(_red, _green, _blue);  
    strip.setPixelColor(led, _color);     
  }      
}


//** decipher the state of encoded sensacell pixel using an index
int getScPxlState(int pxl, char scPxls[], int packetsReceived){
  int pxlByte = pxl / 4;                          // which byte
  int pxlIndex = pxl % 4;                         // which pixel in byte

  int state = 0;
  if(pxlByte < packetsReceived){
    int rowData = ahtoi(scPxls[pxlByte]);
    state = (rowData >> pxlIndex) & 1;
  }

  return state;
}


//** check if data is a command, and check which one.
int commandCheck(char _raw[]){
  boolean validFooter_Flag = false;          // Valid Address Flag

  if( strncmp(&_raw[4],VALID_FOOTER,3) == 0 ){    // used with Color Mode Pointer, and Color Mode Value Commands
    validFooter_Flag = true;
  }

  if(validFooter_Flag){
    int data = getByte(_raw,0);     
    switch(data){
      
    case SENSOR_READ_COMMAND:
      return SENSOR_READ_COMMAND;
      break;

    case COLOR_POINTER_COMMAND:
      return COLOR_POINTER_COMMAND;
      break;

    case COLOR_VALUE_COMMAND:
      return COLOR_VALUE_COMMAND;
      break;

    default:
      return -1;
    }
  }  
}


//** convert ascii hex values to numeric byte
int getByte(char _hexCode[], int index){
  int _data = (ahtoi(_hexCode[index]) << 4) + (ahtoi(_hexCode[index + 1])); 

  return _data;
} 


//** Ascii Hex to Integer Conversion
int ahtoi(char oneChar){        
  int converted = -1;

  if ((oneChar >= '0') && (oneChar <= '9')) {
    converted = oneChar - '0';
  } 
  else if ((oneChar >= 'A') && (oneChar <= 'F')) {
    converted = 10 + oneChar - 'A';
  }
  return converted;
}


//** reverse the order of the bytes for each pack of 4
void reverseByteOrder(char _raw[], int _length){
  for(int i = 0; i < _length; i+=4){
    char rotator[4];
    for(int j = 0; j < 4; j++){
      rotator[j] = _raw[i+3-j];
    }
    memcpy(&_raw[i], rotator, 4);
  }
}


//** save the color value
int parseColor(int _pointer, int _value){
  if(_pointer == IDLE_RED_L2){
    idleL2_raw[0] = _value;
    EEPROM.write(IDLE_RED_L2, _value);
//    Serial.print("idle red L2 = ");
//    Serial.println(_value);
  }
  if(_pointer == IDLE_GREEN_L2){
    idleL2_raw[1] = _value;    
    EEPROM.write(IDLE_GREEN_L2, _value);    
//    Serial.print("idle green L2 = ");
//    Serial.println(_value);    
  }
  if(_pointer == IDLE_BLUE_L2){
    idleL2_raw[2] = _value;    
    EEPROM.write(IDLE_BLUE_L2, _value);    
//    Serial.print("idle blue L2 = ");
//    Serial.println(_value);    
  }
  if(_pointer == ACTIVE_RED_L2){
    activeL2_raw[0] = _value;  
    EEPROM.write(ACTIVE_RED_L2, _value);    
//    Serial.print("active red L2 = ");
//    Serial.println(_value);    
  } 
  if(_pointer == ACTIVE_GREEN_L2){
    activeL2_raw[1] = _value;   
    EEPROM.write(ACTIVE_GREEN_L2, _value);       
//    Serial.print("active green L2 = ");
//    Serial.println(_value);    
  }   
  if(_pointer == ACTIVE_BLUE_L2){
    activeL2_raw[2] = _value;      
    EEPROM.write(ACTIVE_BLUE_L2, _value);       
//    Serial.print("active blue L2 = ");
//    Serial.println(_value);    
  }     

  if(_pointer == IDLE_RED_L1){
    idleL1_raw[0] = _value;
    EEPROM.write(IDLE_RED_L1, _value);
    //    Serial.print("idle red L1 = ");
    //    Serial.println(_value);
  }
  if(_pointer == IDLE_GREEN_L1){
    idleL1_raw[1] = _value;   
    EEPROM.write(IDLE_GREEN_L1, _value); 
    //    Serial.print("idle green L1 = ");
    //    Serial.println(_value);    
  }
  if(_pointer == IDLE_BLUE_L1){
    idleL1_raw[2] = _value;    
    EEPROM.write(IDLE_BLUE_L1, _value);
    //    Serial.print("idle blue L1 = ");
    //    Serial.println(_value);    
  }
  if(_pointer == ACTIVE_RED_L1){
    activeL1_raw[0] = _value;   
    EEPROM.write(ACTIVE_RED_L1, _value);       
    //    Serial.print("active red L1 = ");
    //    Serial.println(_value);    
  } 
  if(_pointer == ACTIVE_GREEN_L1){
    activeL1_raw[1] = _value;       
    EEPROM.write(ACTIVE_GREEN_L1, _value);       
    //    Serial.print("active green L1 = ");
    //    Serial.println(_value);    
  }   
  if(_pointer == ACTIVE_BLUE_L1){
    activeL1_raw[2] = _value;     
    EEPROM.write(ACTIVE_BLUE_L1, _value);       
    //    Serial.print("active blue L1 = ");
    //    Serial.println(_value);    
  }    

  setColorOffsets();
}  


//** save the color value
int initColors(){
  idleL2_raw[0] = EEPROM.read(IDLE_RED_L2);
  idleL2_raw[1] = EEPROM.read(IDLE_GREEN_L2);     
  idleL2_raw[2] = EEPROM.read(IDLE_BLUE_L2);   

  activeL2_raw[0] = EEPROM.read(ACTIVE_RED_L2);   
  activeL2_raw[1] = EEPROM.read(ACTIVE_GREEN_L2);    
  activeL2_raw[2] = EEPROM.read(ACTIVE_BLUE_L2);  

  idleL1_raw[0] = EEPROM.read(IDLE_RED_L1);
  idleL1_raw[1] = EEPROM.read(IDLE_GREEN_L1);     
  idleL1_raw[2] = EEPROM.read(IDLE_BLUE_L1);   

  activeL1_raw[0] = EEPROM.read(ACTIVE_RED_L1);   
  activeL1_raw[1] = EEPROM.read(ACTIVE_GREEN_L1);    
  activeL1_raw[2] = EEPROM.read(ACTIVE_BLUE_L1);  
  
  setColorOffsets();
}  


//** adjust colors based on pot input
void setColorOffsets(){
  idleL2_offset[0] = 256-(analogRead(IDLE_L2_RED_POT)/2);
  idleL2_offset[1] = 256-(analogRead(IDLE_L2_GREEN_POT)/2);  
  idleL2_offset[2] = 256-(analogRead(IDLE_L2_BLUE_POT)/2);  
  activeL2_offset[0] = 256-(analogRead(ACTIVE_L2_RED_POT)/2);
  activeL2_offset[1] = 256-(analogRead(ACTIVE_L2_GREEN_POT)/2);  
  activeL2_offset[2] = 256-(analogRead(ACTIVE_L2_BLUE_POT)/2); 

  updateColorBuffers(); 
}  


//** update the color buffers, and values that will be output to the strips.
void updateColorBuffers(){
  idleL2[0] = idleL2_raw[0] + idleL2_offset[0];
  idleL2[1] = idleL2_raw[1] + idleL2_offset[1]; 
  idleL2[2] = idleL2_raw[2] + idleL2_offset[2];

  idleL2[0] = constrain(idleL2[0], 0, 255);
  idleL2[1] = constrain(idleL2[1], 0, 255);
  idleL2[2] = constrain(idleL2[2], 0, 255);

  idleL2_color = strips[0].Color(idleL2[0], idleL2[1], idleL2[2]);

  activeL2[0] = activeL2_raw[0] + activeL2_offset[0];
  activeL2[1] = activeL2_raw[1] + activeL2_offset[1]; 
  activeL2[2] = activeL2_raw[2] + activeL2_offset[2];

  activeL2[0] = constrain(activeL2[0], 0, 255);
  activeL2[1] = constrain(activeL2[1], 0, 255);
  activeL2[2] = constrain(activeL2[2], 0, 255);  

  activeL2_color = strips[0].Color(activeL2[0], activeL2[1], activeL2[2]);
  
//  printColorBuffers();
}  


//** Dim the strips according to pot
void setBrightnessLevel(){
  bright = 256 - analogRead(BRIGHTNESS_POT)/4; 
  bright = constrain(bright, 100, 255);          //** constrained because if Pixels are off they won't turn back on without reset
  for(int i = 0; i < STRIPS_TOTAL; i++){
    strips[i].setBrightness(bright);
//    strips[i].show(); // Initialize all pixels to 'off'
  } 
}


//** send a color to all the strips
void setStripsColor(uint32_t _color){
  for(int i = 0; i < STRIPS_TOTAL; i++){
    for(int j = 0; j < strips[i].numPixels(); j++){
      strips[i].setPixelColor(j, _color);
    }
  }  
}


//** initialize all the strips
void initStrips(){
  for(int i = 0; i < STRIPS_TOTAL; i++){
    strips[i].begin();
  }
}


//** initialize all the strips
void showStrips(){
  for(int i = 0; i < STRIPS_TOTAL; i++){
    strips[i].show();
  }
}


//** For testing only...
//** Run a test pattern
void test(){
uint32_t red = strips[0].Color(15, 0, 0); 
  uint32_t green = strips[0].Color(0, 15, 0);
  uint32_t blue = strips[0].Color(0, 0, 15);
  uint32_t white = strips[0].Color(10, 10, 10);  
  
  while(1){    
    for(int strip = 0; strip < STRIPS_TOTAL; strip++){       // for all the strips
      colorWipe(strips[strip], red, 0); // Red    
      colorWipe(strips[strip], green, 0); // Red    
      colorWipe(strips[strip], blue, 0); // Red 
//      Serial.println(freeRam());
    }
  }  
}


//** For testing only...
//** Fill the dots one after the other with a color
void colorWipe(Adafruit_NeoPixel& strip, uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();    
    delay(wait);
  }
}


//** for debugging only
void printBuffer(char _buffer[], int _bufferLength){
  for(int b = 0; b < _bufferLength; b++){
    Serial.print(_buffer[b]);
  }
  Serial.println(); 
}  


//** for debugging only
void printColorBuffers(){
  Serial.print(idleL2[0]);
  Serial.print(','); 
  Serial.print(idleL2[1]);
  Serial.print(',');   
  Serial.print(idleL2[2]);  
  Serial.print(' ');  
  Serial.print(activeL2[0]);
  Serial.print(',');   
  Serial.print(activeL2[1]);
  Serial.print(',');  
  Serial.println(activeL2[2]);    
}


//** for debugging only
//** compare fade colors to saved values
void printColorComparison(Adafruit_NeoPixel& strip, int led){

  uint32_t _color = strip.getPixelColor(led);
  int _red = (_color >> 16) & 0xFF;
  int _green = (_color >> 8) & 0xFF;
  int _blue = _color & 0xFF;     
  
//  Serial.println(_color);

  Serial.print(_red);
  Serial.print(",");
  Serial.print(idleL2[0]);  
  Serial.print(" ");
  Serial.print(_green);
  Serial.print(",");
  Serial.print(idleL2[1]);  
  Serial.print(" ");
  Serial.print(_blue);
  Serial.print(",");
  Serial.println(idleL2[2]);    
  Serial.println();            
}


//** determine how much RAM is left...(print this)
int freeRam () 
{
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}


void testPixels(){
  raw[140] = 1;
  raw[136] = 0;
  raw[132] = 1;
  raw[128] = 0;
  raw[124] = 1;
  lightUp(raw, 300);                        // got the Sensacell Sensor data, now go light up the NeoPixels  
}










