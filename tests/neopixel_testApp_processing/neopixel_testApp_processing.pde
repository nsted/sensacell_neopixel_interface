import processing.serial.*;

final int TILES = 145; 
final int sensors = (4 * TILES) + 1;    // The total number of sensors, plus one for the EOF
Serial sensacellPort;  // Create object from Serial class
Serial neopixelPort;  // Create object from Serial class

int val;      // Data received from the serial port
boolean startup = true;

void setup() 
{
  size(200, 200);
  for (int i = 0; i < Serial.list ().length; i++) {
    println(i, " ", Serial.list()[i]);
  }
  println("starting Serial");  
  String portName;
//  portName = Serial.list()[0];
//  neopixelPort = new Serial(this, portName, 250000);
//  neopixelPort.clear();

  portName = Serial.list()[0];
  sensacellPort = new Serial(this, portName, 230400);
  sensacellPort.clear();

//  initColors();
}

int count = 0;

void draw() {
  if (startup) {
    delay(2000);
//    topSensa();  
//    bottomSensa();
//    topNeo();
//    bottomNeo();  
    startup = false;
    println("startup");
    delay(50);

//    while ( neopixelPort.available () > 0) {
//      char debug = char(neopixelPort.read());
//      print(debug);
//    }
  }

  sensacellPort.write("00" + hex(TILES,2) + "a00" + char(0x0D));
//  delay(100);

  readFromSensacellPort();
//  readFromNeopixelPort();
}

// will not print command as part of stream, but can use with one port.
void readFromSensacellPort() {
  while ( sensacellPort.available () > 0) {
    char rx = char(sensacellPort.read());
//    neopixelPort.write(dat);
    print(rx);
  }  
}

// will print command as part of stream, but requires a second port.
void readFromNeopixelPort() {
  while ( neopixelPort.available () > 0) {
    char debug = char(neopixelPort.read());
    print(debug);
  }
}

