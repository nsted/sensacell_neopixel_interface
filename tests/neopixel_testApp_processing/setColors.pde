int idleRed_L1;
int idleGreen_L1;
int idleBlue_L1;

int activeRed_L1;
int activeGreen_L1;
int activeBlue_L1;

int idleRed_L2;
int idleGreen_L2;
int idleBlue_L2;

int activeRed_L2;
int activeGreen_L2;
int activeBlue_L2;

void initColors() {
  int maxColor = 100;
  idleRed_L1 = (int)random(maxColor);
  idleGreen_L1 = (int)random(maxColor);
  idleBlue_L1 = (int)random(maxColor);

  activeRed_L1 = (int)random(maxColor);
  activeGreen_L1 = (int)random(maxColor);
  activeBlue_L1 = (int)random(maxColor);

  idleRed_L2 = (int)random(maxColor);
  idleGreen_L2 = (int)random(maxColor);
  idleBlue_L2 = (int)random(maxColor);

  activeRed_L2 = (int)random(maxColor);
  activeGreen_L2 = (int)random(maxColor);
  activeBlue_L2 = (int)random(maxColor);  
}
  
void topSensa() {
//  sensacellPort.write("1803a00" + char(0x0D));
//  sensacellPort.write("19" + hex(idleRed_L1,2) + "a00" + char(0x0D));
//  delay(100);
//  
//  sensacellPort.write("1804a00" + char(0x0D));
//  sensacellPort.write("19" + hex(idleGreen_L1,2) + "a00" + char(0x0D));
//  delay(100);
//  
//  sensacellPort.write("1805a00" + char(0x0D));
//  sensacellPort.write("19" + hex(idleBlue_L1,2) + "a00" + char(0x0D));
//  delay(100);
  
  sensacellPort.write("1806a00" + char(0x0D));
  sensacellPort.write("19" + hex(idleRed_L2,2) + "a00" + char(0x0D));
  delay(100);
  
  sensacellPort.write("1807a00" + char(0x0D));
  sensacellPort.write("19" + hex(idleGreen_L2,2) + "a00" + char(0x0D));
  delay(100);
  
  sensacellPort.write("1808a00" + char(0x0D));
  sensacellPort.write("19" + hex(idleBlue_L2,2) + "a00" + char(0x0D));
  delay(100);
}


void topNeo() {
//  neopixelPort.write("1803a00" + char(0x0D));
//  neopixelPort.write("19" + hex(idleRed_L1,2) + "a00" + char(0x0D));
//  delay(100);
//
//  neopixelPort.write("1804a00" + char(0x0D));
//  neopixelPort.write("19" + hex(idleGreen_L1,2) + "a00" + char(0x0D));
//  delay(100);
//
//  neopixelPort.write("1805a00" + char(0x0D));
//  neopixelPort.write("19" + hex(idleBlue_L1,2) + "a00" + char(0x0D));
//  delay(100);

  neopixelPort.write("1806a00" + char(0x0D));
  neopixelPort.write("19" + hex(idleRed_L2,2) + "a00" + char(0x0D));
  delay(100);

  neopixelPort.write("1807a00" + char(0x0D));
  neopixelPort.write("19" + hex(idleGreen_L2,2) + "a00" + char(0x0D));
  delay(100);

  neopixelPort.write("1808a00" + char(0x0D));
  neopixelPort.write("19" + hex(idleBlue_L2,2) + "a00" + char(0x0D));
  delay(100);
}

void bottomSensa() {
//  sensacellPort.write("180Ca00" + char(0x0D));
//  sensacellPort.write("19" + hex(activeRed_L1,2) + "a00" + char(0x0D));
//  delay(100);
//  
//  sensacellPort.write("180Da00" + char(0x0D));
//  sensacellPort.write("19" + hex(activeGreen_L1,2) + "a00" + char(0x0D));
//  delay(100);
//  
//  sensacellPort.write("180Ea00" + char(0x0D));
//  sensacellPort.write("19" + hex(activeBlue_L1,2) + "a00" + char(0x0D));
//  delay(100);
  
  sensacellPort.write("180Fa00" + char(0x0D));
  sensacellPort.write("19" + hex(activeRed_L2,2) + "a00" + char(0x0D));
  delay(100);
  
  sensacellPort.write("1810a00" + char(0x0D));
  sensacellPort.write("19" + hex(activeGreen_L2,2) + "a00" + char(0x0D));
  delay(100);
  
  sensacellPort.write("1811a00" + char(0x0D));
  sensacellPort.write("19" + hex(activeBlue_L2,2) + "a00" + char(0x0D));
  delay(100);
}

void bottomNeo() {
  
//  neopixelPort.write("180Ca00" + char(0x0D));
//  neopixelPort.write("19" + hex(activeRed_L1,2) + "a00" + char(0x0D));
//  delay(100);
//
//  neopixelPort.write("180Da00" + char(0x0D));
//  neopixelPort.write("19" + hex(activeGreen_L1,2) + "a00" + char(0x0D));
//  delay(100);
//
//  neopixelPort.write("180Ea00" + char(0x0D));
//  neopixelPort.write("19" + hex(activeBlue_L1,2) + "a00" + char(0x0D));
//  delay(100);

  neopixelPort.write("180Fa00" + char(0x0D));
  neopixelPort.write("19" + hex(activeRed_L2,2) + "a00" + char(0x0D));
  delay(100);

  neopixelPort.write("1810a00" + char(0x0D));
  neopixelPort.write("19" + hex(activeGreen_L2,2) + "a00" + char(0x0D));
  delay(100);

  neopixelPort.write("1811a00" + char(0x0D));
  neopixelPort.write("19" + hex(activeBlue_L2,2) + "a00" + char(0x0D));
  delay(100);    
}

